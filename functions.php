<?php
/**
 * Functions and definitions
 *
 */
function yannis_enqueue_styles() {

  wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/assets/bootstrap/node_modules/bootstrap/compiled/bootstrap.css' );
  wp_enqueue_style( 'font_awsome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css' );
  wp_enqueue_style( 'google_web_fonts', 'https://fonts.googleapis.com/css?family=Poppins:300,400,400i,700' );
  wp_enqueue_style( 'core', get_template_directory_uri() . '/style.css' );

}
add_action( 'wp_enqueue_scripts', 'yannis_enqueue_styles');

function yannis_enqueue_scripts() {
  wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array( 'jquery' ) );
  wp_register_script( 'popper', 'https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', false, '', true); 
  wp_enqueue_script( 'popper' );

}
add_action( 'wp_enqueue_scripts', 'yannis_enqueue_scripts');


/* Theme Options */

add_theme_support( 'menus' );


/* Menus */
register_nav_menus( 
  array(

 		'top-menu' => "Top Menu Location",
 		'mobile-menu' => "Mobile Menu Location"

 	)
  );

/* Register Custom Navigation Walker */
function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );

if ( ! file_exists( get_template_directory() . '/class-wp-bootstrap-navwalker.php' ) ) {
    // File does not exist... return an error.
    return new WP_Error( 'class-wp-bootstrap-navwalker-missing', __( 'It appears the class-wp-bootstrap-navwalker.php file may be missing.', 'wp-bootstrap-navwalker' ) );
} else {
    // File exists... require it.
    require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}

/* Limit excerpt and change excerpt more */

  function get_excerpt(){
    $excerpt = get_the_content();
    $excerpt = preg_replace(" ([.*?])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, 100);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
    $excerpt = $excerpt.'… <a class="font-weight-bolder mt-2" href="'. get_permalink($post->ID) . '">' . '<br><br>READ MORE &rarr;' . '</a>';
    return $excerpt;
  }  

   


   /* Thumbnails */
   add_theme_support( 'post-thumbnails' );


  /* Ajax */

   add_action('wp_ajax_enquiry', 'enquiry_form');
   add_action('wp_ajax_nopriv_enquiry', 'enquiry_form');
  


   function enquiry_form(){

     $formdata = [];
     wp_parse_str( $_POST['enquiry'], $formdata);    
    

     

       try{

         if(!empty($formdata)){


          wp_send_json_success($formdata['fname'].', thank you for sending message to our company');

         } 
           else {

            wp_send_json_error('Error');

          }

      } catch(Exception $ex){

          wp_send_json_error( $ex->getMessage() );

       }

    
   }