<?php get_header();?>


<?php ?>

<section>

	<div class="container mt-5">
		<div class="row">			

			<div class="col-sm-10 offset-sm-1 ">
			  <h4 class="text-dark font-weight-bold">Tips from our blog</h4>
				<div class="card-deck pt-5">					



					 <?php 
						$args = array(
										'category_name' => 'Tips',
										'posts_per_page' => 3,
										'offset' => 0
									);						

						$the_query = new WP_Query( $args );

					?>


					 

					<?php while ($the_query -> have_posts()) : $the_query -> the_post();?>
					<?php 
						$post_id = get_the_ID();
						$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' );
						 
					?>
							
							
									<div class="card shadow">
									    <img class="card-img-top img-fluid" src="<?php echo $url ?>">
									        <div class="card-body">
										        <h5 class="card-title text-info"><?php the_title(); ?></h5>
										        <div>
											        <i class="far fa-clock text-muted"></i>
											        <small class="text-muted"> <?php the_time('d-m-Y'); ?></small><br><br>
											        </div>
										        <a class="card-text text-secondary"><?php echo get_excerpt(); ?></a>
										    </div>
				    				</div> 


							
						

						<?php endwhile; ?>
						<?php wp_reset_postdata();?>


					
				</div> <!-- card-decks -->

				<div class="card-deck pt-5">




						<?php 
						$args = array(
										'category_name' => 'Tips',
										'posts_per_page' => 3,
										'offset' => 3
									);						

						$the_query = new WP_Query( $args );

					?>

						<?php while ($the_query -> have_posts()) : $the_query -> the_post();?>

						<?php 
							$post_id = get_the_ID();
							$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' );
							 
						?>
								
								
									<div class="card shadow">
									    <img class="card-img-top img-fluid" src="<?php echo $url ?>">
									        <div class="card-body">
										        <h5 class="card-title text-info"><?php the_title(); ?></h5>
										        <div>
											        <i class="far fa-clock text-muted"></i>
											        <small class="text-muted"> <?php the_time('d-m-Y'); ?></small><br><br>
											        </div>
										        <a class="card-text text-secondary"><?php echo get_excerpt(); ?></a>
										    </div>
				    				</div> 
								
							

							<?php endwhile; ?>
							<?php wp_reset_postdata();?>

					
				</div> <!-- card-decks -->

				

			</div>


		</div>
		

	</div> <!-- container -->

</section>




<section id = "contact" class="bg-light pb-5 pt-5 mt-5">

	<div class="container pb-5 pt-5">
	  
		
		<div class="row">
			<div class="col-lg-6 offset-1">
				<h4 class="mb-5 font-weight-bold">Contact Form</h4>
				
				<form id="enquiry">
				  <div class="form-group row">
				    <div class="col-lg-6">
				      <label class="text-info">YOUR FIRST NAME *</label>
				      <input type="text" class="form-control" name="fname" placeholder="Enter your first name" required>
				    </div>
				    <div class="col-lg-6">
				      <label class="text-info">YOUR LAST NAME *</label>
				      <input type="text" class="form-control" name="lname" placeholder="Enter your first name" required>
				    </div>				    
				  </div>

				  <div class="form-group">
		                <label class="text-info" for="email">YOUR EMAIL *</label>
		                <input class="form-control form-control" type="email" name="email" placeholder="Enter your email" required>	                
                  </div>
				  <div class="form-group">
		                <label class="text-info" for="message">YOUR MESSAGE FOR US *</label>
		                <textarea class="form-control" id="message" rows="3" placeholder="Enter your message" required></textarea>
		           </div>
		           <div class="form-group">
		           		<button type = "submit" class="btn btn-outline-primary">SEND MESSAGE</button>
		           </div>
		           
				</form>
				<div class="alert alert-success bg-light" id="success_message" style="display: none"></div>

				<script>
					(function($){

						$('#enquiry').submit(function(eventPhase){
							
							event.preventDefault();

							var endpoint = '<?php echo admin_url('admin-ajax.php')?>';

							var form = $('#enquiry').serialize();

							var formdata = new FormData;

							formdata.append('action', 'enquiry');
							formdata.append('enquiry', form);

							$.ajax(endpoint, {

								type: 'POST',
								data: formdata,
								processData: false,
								contentType: false, 

								success: function(res){
									$('#enquiry').fadeOut(200);

									var msg = res.data;

									$('#success_message').text(msg).show();

									$('#enquiry').trigger('reset');

									$('#enquiry').fadeIn(500);
								},

								error: function(err){

								}

							})
						})

					})(jQuery);
				</script>


			</div>
			<div class="col-lg-4">
				<h3 class="mb-5"> <br> </h3>
				<p class="text-info">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magnam optio quod excepturi, dolores odio nesciunt aperiam dolore, ea, amet fugit beatae quisquam eos a doloremque expedita ex ab quasi blanditiis cum? Facere excepturi et repellendus doloribus ipsa corrupti, tempora, ullam, eligendi quos illo culpa! Placeat doloremque mollitia unde quaerat, vero.</p>
				<i class="fab fa-twitter mr-3 text-primary"></i><i class="fab fa-facebook-square mr-3 text-primary"></i><i class="fab fa-instagram mr-3 text-primary"></i><i class="fab fa-pinterest mr-3 text-primary"></i><i class="fab fa-vimeo-square mr-3 text-primary"></i>
				
			</div>

		</div>

	  </div> <!-- container -->
	</div> <!-- bg-light div -->

	</div>
</div>
</section>

<?php get_footer();?>
