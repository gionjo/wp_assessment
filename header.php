<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta name="viewport" content="width=device width, initial-scale=1">
    <meta charset="<?php bloginfo('charset');?>">
    <?php wp_head(); ?>
  </head>
  <body>

  	
        <header>	

			<nav class="navbar navbar-expand-lg navbar-light bg-white" role="navigation">
			  
			    <a class="navbar-brand" href="#"><img src="<?php echo get_theme_file_uri('assets/img/logo.png') ?>" alt="" width="90%" height="90%"></a>
			    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-controls="bs-example-navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
			        <span class="navbar-toggler-icon"></span>
			    </button>
			    
			        <?php
			        wp_nav_menu( array(
			            'theme_location'    => 'top-menu',
			            'depth'             =>  2,
			            'container'         => 'div',
			            'container_class'   => 'collapse navbar-collapse justify-content-end',
			            'container_id'      => 'bs-example-navbar-collapse-1',
			            'menu_class'        => 'nav navbar-nav text-secondary font-weight-bolder mb-1',
			            'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
			            'walker'            => new WP_Bootstrap_Navwalker(),
			        ) );
			        ?>
			   <form class="form-inline my-2 my-lg-0">			      
			      <button class="btn btn-primary my-2 my-sm-0">REGISTER</button>
			   </form>
 		    </nav>
        </header>      		
 		
    
 
<div class="jo-header">
     
 		<div class="container h-100">
   		 	<div class="row h-100 align-items-center">
         	 	<div class="col-12 text-center">
          		 <h6 class="text-white">Home &ndash; Contact</h6>
          		 <h2 class="text-white font-weight-bold">How can we help you today?</h2>
       		 	</div>
  		   </div>
 		 </div>
</div> 		
	

