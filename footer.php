	<footer id="footer-main">
        <div class="container mt-5">		
	
            <div class="row">
	         
                <div class="col-sm-3 offset-sm-1">
                    <h6 class="text-dark font-weight-bold pb-3">COMPANY</h6>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
                    <p><i class="fab fa-twitter mr-3 text-muted"></i><i class="fab fa-facebook-square mr-3 text-muted"></i><i class="fab fa-instagram mr-3 text-muted"></i><i class="fab fa-pinterest mr-3 text-muted"></i><i class="fab fa-vimeo-square mr-3 text-muted"></i></p>
                </div>
                <div class="col-sm-2">
                    <h6 class="text-dark font-weight-bold pb-3">ABOUT</h6>
                    <p class="text-muted">Lorem<br>Ipsum</p>                    
                </div>
                <div class="col-sm-2">
                    <h6 class="text-dark font-weight-bold pb-3">TERMS</h6>
                    <p class="text-muted">Lorem<br>Ipsum</p> 
                </div>
                <div class="col-sm-3">
                    <h6 class="text-dark font-weight-bold pb-3">FIND US</h6>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam expedita fugit saepe.</p>
                </div>
            </div>
	</div> 

    </footer>

	<div class="bg-dark text-info text-center pt-3 pb-3"><small>&copy; 2020 Company. All rights reserved</small></div>
<?php wp_footer(); ?>
</body>
</html>