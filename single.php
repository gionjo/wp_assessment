<?php 
wp_head();
 while(have_posts()) {
    the_post(); 
    ?>
   
  <div class="container">
     		<?php the_post_thumbnail(); ?>
            <p class="text-info"> 
            <span class="text-info">Posted on <?php the_date('d-m-Y')?> in <?php echo get_the_category_list(',')?></span></p>
      
      <p><?php the_content(); ?></p>
  </div>

    
  <?php wp_footer(); }